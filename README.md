# its-menu
## Include This Script
### About
its-menu aims to offer simple way to exicute bash commands into self generating bash menu by searching for moduale called `*.its` 

its contain simple to complex bash functions and double comment tags to identify menu items.

## Usage
Clone the repo
```bash
git clone https://github.com/Tearran/its-menu.git
```
Create a libits examples in its-menu/libits folder the run
```bash
bash its-menu -m 
# The -m flag runs makelib to genirate the menu
```
# Other flags
```bash
Usage: its-menu [ -h  | -v | -m ]
Options:
        -h      Print this help.
        -v      Print version info.
        -m      makelib Generate libits library.
```
## Target system
Debian on a Single board computor
### Tested on
- Rapberry pi Zero
- Rapberry pi Zero W
- Rapberry pi Zero 2
- OS Raspian Buster (long term support)



